#include "imagem.hpp"
#include "filtro.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>

using namespace std;

Filtro::Filtro(){
	int filter[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	setFilter(filter);
}

Filtro::Filtro(int filter[]){
	setFilter(filter);
}

int* Filtro::getFilter(){
	return filter;
}

void Filtro::setFilter(int filter[]){
	for(int i = 0; i < 9; i++){
		this->filter[i] = filter[i];
	}
}
void Filtro::setDivisao(int divisao){
	this->divisao=divisao;
}

void Filtro::aplicarNegativo(Imagem image){

}

void Filtro::aplicarFiltro(Imagem image){
	int i, j, x, y;
	int limit = 0;
	int *pixels = image.getPixels();
	int *copyPixels = new int[image.getAltura()*image.getLargura()];
	for(i = limit; i < image.getLargura() - limit; i++){
		for ( j = limit; j < image.getAltura() - limit ; j++){
			int value = 0;
			for ( x = -1; x <= 1; x++){
				for ( y = -1; y <= 1; y++){
					value += this->filter[( x+1) + 3 * ( y+1)] * pixels[ ( i + x ) + ( y + j ) * image.getLargura() ] ;
				}
			}
		value /= this->divisao ;
		value = value < 0 ? 0 : value ;
		value = value > 255 ? 255 : value ;
		copyPixels[ i + j * image.getLargura()] = value ;
		}
	}
	image.setPixels(copyPixels);
	string novoCaminho;
	cout << "Digite novo caminho para imagem filtrada" << endl;
	cin >> novoCaminho;
	image.salvarImagem(novoCaminho.c_str());
}