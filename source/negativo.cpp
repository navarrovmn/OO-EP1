#include "negativo.hpp"
#include <iostream>

using namespace std;

Negativo::Negativo(){
	int filter[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	setFilter(filter);
}

void Negativo::aplicarNegativo(Imagem image){
	int i;
	int *pixels = image.getPixels();
	int *copyPixels = new int[image.getAltura()*image.getLargura()];
	for(i = 0; i < image.getLargura() * image.getAltura(); i++){
		copyPixels[i] = image.getCinzaMaximo() - pixels[i];
	}
	image.setPixels(copyPixels);
	string novoCaminho;
	cout << "Digite novo caminho para imagem filtrada" << endl;
	cin >> novoCaminho;
	image.salvarImagem(novoCaminho.c_str());
}