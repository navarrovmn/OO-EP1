#include "imagem.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>

using namespace std;

Imagem::Imagem(){
	nome = "";
	caminhoImagem = "";
	pixels = NULL;
	altura = 0;
	largura = 0;
	cinzaMaximo = 0;
}
Imagem::Imagem(std::string nome, std::string caminhoImagem, int altura, int largura, int cinzaMaximo){
	setNome(nome);
	setCaminhoImagem(caminhoImagem);
	setAltura(altura);
	setLargura(largura);
	setCinzaMaximo(cinzaMaximo);
}

std::string Imagem::getNome(){
	return nome;
}
std::string Imagem::getCaminhoImagem(){
	return caminhoImagem;
}
int * Imagem::getPixels(){
	return pixels;
}
int Imagem::getAltura(){
	return altura;
}
int Imagem::getLargura(){
	return largura;
}
int Imagem::getCinzaMaximo(){
	return cinzaMaximo;
}
void Imagem::setNome(std::string nome){
	this->nome = nome;
}
void Imagem::setCaminhoImagem(std::string caminhoImagem){
	this->caminhoImagem = caminhoImagem;
}
void Imagem::setAltura(int altura){
	this->altura = altura;
}
void Imagem::setLargura(int largura){
	this->largura = largura;
}
void Imagem::setCinzaMaximo(int cinzaMaximo){
	this->cinzaMaximo = cinzaMaximo;
}
void Imagem::setPixels(int *pixels){
	int i,j;
	int pixelVale;
	int altura = getAltura();
	int largura = getLargura();
	for(i=0; i<altura; i++){
		for(j=0; j<largura; j++){
			pixelVale = pixels[i*largura+j];
			this->pixels[i*largura+j] = pixelVale;
		}
	}
}
void Imagem::lerImagem(const char fname[]){
	int i, j;
	unsigned char *charImage;
	char header [100], *ptr;
	ifstream ifp;

	ifp.open(fname, ios::in | ios::binary);

	if(!ifp){
		cout<< "Nao e possivel ler imagem" << endl;
		exit(1);
	}
	ifp.getline(header, 100, '\n');
	if( (header[0]!=80) || (header[1]!= 53) ){
		cout<<"A imagem nao e PGM" << endl;
		exit(1);
	}
	ifp.getline(header, 100, '\n');
	while(header[0]=='#')
		ifp.getline(header,100,'\n');

	int altura = strtol(header, &ptr, 0);
	int largura = atoi(ptr);
	setAltura(altura);
	setLargura(largura);

	ifp.getline(header,100,'\n');
	int cinzaMaximo=strtol(header,&ptr,0);
	setCinzaMaximo(cinzaMaximo);

	this->pixels = new int[altura*largura];
	charImage = (unsigned char *) new unsigned char [altura*largura];
	ifp.read( reinterpret_cast<char *>(charImage), (altura*largura)*sizeof(unsigned char));
	ifp.close();
	int pixelVale;
	for(i=0; i<altura; i++){
		for(j=0; j<largura; j++){
			pixelVale = (int) charImage[i*largura+j];
			this->pixels[i*largura+j] = pixelVale;
		}
	}
}
void Imagem::salvarImagem(const char fname[]){
	int i, j;
	unsigned char *charImage;
	ofstream outfile(fname);

	charImage = (unsigned char *) new unsigned char [altura*largura];

	int val;

	for(i=0; i<altura; i++){
		for(j=0; j<largura; j++){
			val = pixels[i*largura+j];
			charImage[i*largura+j]=(unsigned char)val;
		}
	}

	if (!outfile.is_open()){
		cout << "Can't open output file"  << fname << endl;
		exit(1);
	}

	outfile << "P5" << endl;
	outfile << altura << " " << largura << endl;
	outfile << 255 << endl;

	outfile.write(reinterpret_cast<char *>(charImage), (altura*largura)*sizeof(unsigned char));

	cout << "Arquivo gerado" << endl;
	outfile.close();
}