#include "imagem.hpp"
#include "imagem.hpp"
#include "filtro.hpp"
#include "sharpen.hpp"
#include "smooth.hpp"
#include "negativo.hpp"
#include <iostream>
#include <string>


using namespace std;

int main(){
	Imagem imagem;

	string caminho;

	cout << "Digite o caminho da imagem" << endl;

	cin >> caminho;
	imagem.lerImagem(caminho.c_str());

	string response;
	cout << "Gostaria de: \n" <<
				"(1)- Salvar em outro Lugar \n" <<
				"(2)- Aplicar filtro Sharpen \n" <<
				"(3)- Aplicar filtro Smooth \n" <<
				"(4)- Aplicar filtro negativo" << endl;
	cin >> response;

	if(response == "1"){
		cout << "Digite novo caminho" << endl;
		string novoCaminho;
		cin >> novoCaminho;
		imagem.salvarImagem(novoCaminho.c_str());
	}else if(response == "2"){
		cout << "Filtro Sharpen" << endl;
		Sharpen sharpen;
		sharpen.aplicarFiltro(imagem);
	}else if(response=="3"){
		cout << "Filtro Smooth" << endl;
		Smooth smooth;
		smooth.aplicarFiltro(imagem);
	}else if(response == "4"){
		cout << "Filtro negativo" << endl;
		Negativo negativo;
		negativo.aplicarNegativo(imagem);
	}
}
