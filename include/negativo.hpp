#ifndef NEGATIVO_H
#define NEGATIVO_H

#include "filtro.hpp"

class Negativo : public Filtro{
	public:
	Negativo();
	void aplicarNegativo(Imagem image);
};

#endif