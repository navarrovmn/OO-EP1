#ifndef SHARPEN_H
#define SHARPEN_H

#include "filtro.hpp"

class Sharpen : public Filtro{
	public:
		Sharpen();
		Sharpen(int filter[]);
};

#endif