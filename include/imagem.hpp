#ifndef IMAGEM_H
#define IMAGEM_H
#include <string>

class Imagem{
	private:
		std::string nome;
		std::string caminhoImagem;
		int altura, largura;
		int cinzaMaximo;
		int *pixels;


	public:
		Imagem();
		Imagem(std::string nome, std::string caminhoImagem, int altura, int largura, int cinzaMaximo);
		std::string getNome();
		void setNome(std::string nome);
		std::string getCaminhoImagem();
		void setCaminhoImagem(std::string caminhoImagem);
		int * getPixels();
		void setPixels(int * pixels);
		int getAltura();
		void setAltura(int altura);
		int getLargura();
		void setLargura(int largura);
		int getCinzaMaximo();
		void setCinzaMaximo(int cinzaMaximo);

		void lerImagem(const char fname[]);
		void salvarImagem(const char fname[]);
};

#endif
