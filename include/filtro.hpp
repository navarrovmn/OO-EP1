#ifndef FILTRO_H
#define FILTRO_H
#include "imagem.hpp"

class Filtro{
	private:
		int filter[9];
		int divisao;

	public:
		Filtro();
		Filtro(int filter[]);
		int* getFilter();
		void setFilter(int filter[]);
		void aplicarFiltro(Imagem image);
		void setDivisao(int divisao);
		virtual void aplicarNegativo(Imagem image);
};
#endif

